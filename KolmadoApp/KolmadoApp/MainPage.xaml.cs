﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Xamarin.Forms;

using Plugin.Settings;
using static Xamarin.Forms.Button;
using static Xamarin.Forms.Button.ButtonContentLayout;
using Flex.Controls;
using KolmadoApp.Controls;

namespace KolmadoApp
{
    public partial class MainPage : ContentPage
    {

        List<CategoriesAndProducts> allCategoriesList = new List<CategoriesAndProducts>();
        public MainPage(bool firstStart = false)
        {
            var allCategoriesList = GlobalVars.allCategoriesGlobal;
            List<Product> shoppingCartProducts = HelpingFunctions.getProductsFromProperties();
            App.Current.Properties["shoppingCart"] = shoppingCartProducts;
            CrossSettings.Current.AddOrUpdateValue("shoppingCartCounter", ProductListMethods.CountPieces(shoppingCartProducts));
            Title = "Kategorien";
            #region footer
            ImageButton homeIcon = new ImageButton { ShowedImage = "home.png", ShowedText = "Home" };
            ImageButton ordersIcon = new ImageButton { ShowedImage = "orders.png", ShowedText = "Orders", HeightRequest = 35 };
            ImageButton contactIcon = new ImageButton { ShowedImage = "contact.png", ShowedText = "Contact", HeightRequest = 35 };
            homeIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new MainPage());
                },
                NumberOfTapsRequired = 1
            });
            ordersIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    List<Product> orderProducts = HelpingFunctions.getProductsFromProperties("OrderProducts");
                    App.Current.Properties["OrderProducts"] = orderProducts;
                    if (orderProducts.Count > 0)
                    {
                        Navigation.PushAsync(new DeliveryPage());
                    }
                    else
                    {
                        DisplayAlert("Keine Bestellungen", "Leider haben Sie noch keine Bestellungen getätigt.", "OK");
                    }
                },
                NumberOfTapsRequired = 1
            });
            contactIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new SettingsPage());
                },
                NumberOfTapsRequired = 1
            });

            var footerGrid = new Grid();
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.Children.Add(homeIcon, 0, 0);
            footerGrid.Children.Add(ordersIcon, 1, 0);
            footerGrid.Children.Add(contactIcon, 2, 0);
            #endregion
            ToolbarItem shoppingCartCounterTI = new ToolbarItem
            {
                Command = new Command(async () =>
await Navigation.PushAsync(new ShoppingCartPage())),
                Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + ""
            };
            var shoppingCartIconTI = new ToolbarItem
            {
                Command = new Command(async () =>
                await Navigation.PushAsync(new ShoppingCartPage())),
                Icon = "shoppingCart.png",
                Name = "Einkaufswagen"
            };
            ToolbarItems.Add(shoppingCartIconTI);
            ToolbarItems.Add(shoppingCartCounterTI);
            this.Padding = new Thickness(15, 30, 20, 0);
            //NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            Label header = new Label
            {
                Text = "Kategorien",
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HorizontalOptions = LayoutOptions.Center
            };


            string[] categoryNames = new string[allCategoriesList.Count];
            int categoryListCounter = 0;
            int categoryListGridColumnCounter = 0;
            int categoryListGridRowCounter = 0;
            foreach (var item in allCategoriesList)
            {
                categoryNames[categoryListCounter] = item.products[0].productname;
                categoryListCounter++;
            }
            // Create the ListView.

            Grid categoryListGrid = new Grid
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.StartAndExpand
            };
            categoryListGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            categoryListGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            categoryListGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            for (int i = 0; i < allCategoriesList.Count; i++)
            {
                Label nameLabel = new Label
                {
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    TextColor = Color.Black
                };
                nameLabel.Text = allCategoriesList[i].categoryName;
                Image image = allCategoriesList[i].categoryImage;
                StackLayout imageLabel = new StackLayout
                {
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.StartAndExpand,
                    BindingContext = allCategoriesList[i],
                    Children =
                    {
                        image,
                        nameLabel
                    }
                };
                imageLabel.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    TappedCallback = async (v, o) =>
                    {
                        showProductsPageAsync((CategoriesAndProducts)((StackLayout)v).BindingContext);
                    },
                    NumberOfTapsRequired = 1
                });
                categoryListGrid.Children.Add(imageLabel, categoryListGridColumnCounter, categoryListGridRowCounter);
                if (categoryListGridColumnCounter == 0)
                {
                    categoryListGridColumnCounter = 1;
                }
                else
                {
                    categoryListGridColumnCounter = 0;
                }
                if ((i + 1) % 2 == 0)
                {
                    categoryListGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                    categoryListGridRowCounter++;
                }
            }
            /*
                        ListView categoryList = new ListView
                        {
                            HorizontalOptions = LayoutOptions.FillAndExpand,
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            HasUnevenRows = true,
                            ItemsSource = allCategoriesList,
                            ItemTemplate = new DataTemplate(() =>
                            {
                                Label nameLabel = new Label
                                {
                                    TextColor = Color.Black
                                };
                                nameLabel.SetBinding(Label.TextProperty, "categoryName");
                                Image image = new Image();
                                image.HeightRequest = 100;
                                image.WidthRequest = 100;
                                image.Source = ImageSource.FromFile("kebap.jpg");

                                var grid = new Grid();
                                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
                                grid.Children.Add(nameLabel, 0, 1);
                                grid.Children.Add(image, 0, 0);
                                // Source of data items.
                                return new ViewCell
                                {
                                    View = grid
                                };
                            })
                        };
                        */
            /*  categoryListGrid.Ta += (sender, e) =>
              {
                  ((ListView)sender).SelectedItem = null;
                  var item = (CategoriesAndProducts)e.SelectedItem;
                  showProductsPageAsync(item);
              };*/

            //Zuweißen Zum aktuellen Layout
            // Accomodate iPhone status bar.

            this.Content = new StackLayout
            {
                Children =
                     {
                   // headerLayout,
                         new ScrollView{HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                             Content =categoryListGrid },
                         footerGrid
                     }
            };


            //monochrome will not appear in the list because it was added
            //after the list was populated.
        }
        public async Task showProductsPageAsync(CategoriesAndProducts categoriesAndProducts)
        {
            NavigationPage.SetBackButtonTitle(this, "");
            //NavigationPage.SetTitleIcon(this, "home.png");
            await Navigation.PushAsync(new ProductsPage(categoriesAndProducts));
        }

        protected override void OnAppearing()
        {
            ToolbarItem shoppingCartCounterTI = new ToolbarItem { Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + "" };
            ToolbarItems[1] = shoppingCartCounterTI;

            String openDateMessage = HttpRequests.getOpenDate(GlobalVars.host);
            if (openDateMessage.Equals("") == false) DisplayAlert("Außerhalb der Lieferzeit", openDateMessage, "OK");
        }
        public async Task gotoOrdersPage()
        {
            await Navigation.PushAsync(new DeliveryPage());
        }
        public async Task contactsPage()
        {
            await Navigation.PushAsync(new DeliveryPage());
        }
    }

}
