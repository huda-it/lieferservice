﻿using PayPal.Forms.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace KolmadoApp
{
    class ProductListMethods
    {
        public static bool IsProductInList(String productNameToSearchFor,List<Product> productsToSearchIn)
        {
            foreach (var item in productsToSearchIn)
            {
                if (item.productname==productNameToSearchFor)
                {
                    return true;
                }
            }
            return false;
        }

        public static int FindProductsIndexInList(String productNameToSearchFor, List<Product> productsToSearchIn)
        {
            int i = 0;
            foreach (var item in productsToSearchIn)
            {
                if (item.productname == productNameToSearchFor)
                {
                    return i;
                }
                i = i + 1;
            }
            return -1;
        }
        public static int CountPieces(List<Product> productsToCountPieces)
        {
            int result = 0;
            foreach (var item in productsToCountPieces)
            {
                result = result + item.pieces;
            }
            return result;
        }
        public static double GetTotalPriceOfProducts(List<Product> productsToSum)
        {
            double totalPrice = 0;
            foreach (var item in productsToSum)
            {
                totalPrice += item.pieces * (item.price + item.pfand);
            }
            return totalPrice;
        }
        public static String GetXmlFromProducts(List<Product> productsToConvertToXml)
        {
            String result = "<products>";
            foreach (var item in productsToConvertToXml)
            {
                String productXml=item.ProductToXml();
                result += productXml;
            }
            result += "</products>";
            return result;
        }
        public static Boolean CheckIfAgeIsAllowed(int age,List<Product> allProducts)
        {
            Boolean result = true;
            foreach (Product product in allProducts)
            {
                String fsk = product.fsk + "";
                if (product.fsk > age) result = false;
            }
            return result;
        }
        public static PayPalItem[] GetPaypalItemsFromProducts(List<Product> productsToConvertToPaypalItesm)
        {
            List<PayPalItem> result = new List<PayPalItem>();
            foreach (var item in productsToConvertToPaypalItesm)
            {
                result.Add(new PayPalItem(item.productname,Convert.ToUInt32(item.pieces), new Decimal(item.price), "EUR",
                     "sku-12345678"));
            }
            return result.ToArray();
        }
    }
}
