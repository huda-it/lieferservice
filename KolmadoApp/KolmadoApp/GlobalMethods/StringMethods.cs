﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KolmadoApp
{
    public static class StringMethods
    {
        public static String ValueBetweenStringsInString(String mainString, String startString, String endString)
        {
            String product = "";
            int startStringIndex = mainString.IndexOf(startString, 0) + startString.Length;
            int endStringIndex = mainString.IndexOf(endString, 0);
            while ((startStringIndex - startString.Length) != -1 && endStringIndex != -1)
            {
                product += ";" + mainString.Substring(startStringIndex, endStringIndex - (startStringIndex));
                startStringIndex = mainString.IndexOf(startString, startStringIndex + 1) + startString.Length;
                endStringIndex = mainString.IndexOf(endString, endStringIndex + 1);
            }
            product = product.Remove(0, 1);
            return product;
        }





        public static String ValueBetweenStringsInStringFirstOnly(String mainString, String startString, String endString)
        {
            String product = "";
            int startStringIndex = mainString.IndexOf(startString, 0) + startString.Length;
            int endStringIndex = mainString.IndexOf(endString, 0);
            product = mainString.Substring(startStringIndex, endStringIndex - (startStringIndex));
            return product;
        }

    }
}
