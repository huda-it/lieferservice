﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Xamarin.Forms;

namespace KolmadoApp
{
    public class ProductXmlMethods
    {
        public XmlDocument proudctDoc { get; set; }

        public ProductXmlMethods()
        {
            proudctDoc = new XmlDocument();

        }

        public XmlNode getProductByAttribute(String attributeName, String attributeContent, int itemIndex = 0)
        {
            return proudctDoc.SelectNodes("//product/" + attributeName + "[text()='" + attributeContent + "']").Item(itemIndex);
        }

        public List<CategoriesAndProducts> getAllCategories(string host)
        {
            List<CategoriesAndProducts> result = new List<CategoriesAndProducts>();
            foreach (XmlNode item in proudctDoc.SelectNodes("categories/category"))
            {
                List<Product> productsOfCategoriesList = new List<Product>();

                
                string categoryId = item.Attributes["id"].Value +"";
                foreach (XmlNode item2 in item.SelectNodes("products/product"))
                {
                    Product p = new Product {description=item2.SelectSingleNode("description").InnerText, productname = item2.SelectSingleNode("name").InnerText, piecesTotal = Int32.Parse(item2.SelectSingleNode("pieces").InnerText), price = Double.Parse(item2.SelectSingleNode("price").InnerText.Replace(",",".")), fsk= Int32.Parse(item2.SelectSingleNode("fsk").InnerText), pictureUrl = host+ HelpingFunctions.convertPictureUrl(item2.SelectSingleNode("name").InnerText) +".jpg",pieces=0 };
                    
                    GlobalVars.listOfAllProducts.Add(p);
                    if (item2.SelectSingleNode("pfand")!=null){
                        p.pfand = double.Parse(item2.SelectSingleNode("pfand").InnerXml.Replace(",", "."));
                        double sadf = double.Parse(item2.SelectSingleNode("pfand").InnerXml.Replace(",", "."));
                    }
                    else {
                        p.pfand = 0;
                        }
                    productsOfCategoriesList.Add(p);
                }
                var pictureFormat = item.SelectSingleNode("categoryPictureFormat").InnerText;
                Image categoryImage = new Image
                {
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    HeightRequest=100, //Heighrequest damit alle gleich sind XX noch ändern je nach Device
                    Source = ImageSource.FromUri(new Uri(host + HelpingFunctions.convertPictureUrl(categoryId+"."+pictureFormat)))};
                result.Add(new CategoriesAndProducts { categoryName = categoryId, categoryImage=categoryImage, products = productsOfCategoriesList });
            }
            return result;
        }

        public XmlNodeList getProductsWithAttribute(String attributeName, String attributeContent)
        {
            return proudctDoc.SelectNodes("//product/" + attributeName + "[text()='" + attributeContent + "']");
        }

        public XmlNode getProductsWithAttributeByName(String productName, String attributeToGetName)
        {

            return proudctDoc.SelectNodes("//product/name[text()='" + productName + "']").Item(0).ParentNode.SelectSingleNode(attributeToGetName);
        }


        public List<Product> getAllProductsFromCategoryName(String categoryName)
        {
            List<Product> result = new List<Product>();
            foreach (XmlNode item in proudctDoc.SelectSingleNode("categories/category[@id='" + categoryName + "']/products"))
            {
                Product p = new Product { productname = item.SelectSingleNode("name").InnerText, piecesTotal = Int32.Parse(item.SelectSingleNode("pieces").InnerText), price = Double.Parse(item.SelectSingleNode("price").InnerText) };
                result.Add(p);
            }
            return result;
        }


        public XmlNodeList getAllProducts()
        {
            return proudctDoc.SelectNodes("//product");
        }
        public XmlNodeList getAllProductsName()
        {
            return proudctDoc.SelectNodes("//product/name");
        }


        public static void addTotalPieces(String xml)
        {
            String piecesTag = "<pieces>";
            String piecesEndTag = "</pieces>";
            String productEndTag = "</product>";
            int piecesIndex = xml.IndexOf(piecesTag);
            while (piecesIndex != -1)
            {
                String pieces = xml.Substring(xml.IndexOf(piecesTag) + piecesTag.Length, xml.IndexOf(piecesEndTag));
                if (xml.IndexOf(piecesEndTag) < xml.IndexOf(productEndTag))
                {
                   // MessageBox.Show(pieces);
                }
                piecesIndex = xml.IndexOf(piecesTag);
            }
        }

        public Boolean checkFskFromProductOkay(int age,List<Product> productsToCheck)
        {
            foreach (var item in productsToCheck)
            {
                if (item.fsk > age)
                {
                    return false;
                }
            }
            return true;
        }


        //METHODEN WELCHE DIREKT IN OBJEKT SPEICHERN
        public List<Product> getAllProductsToList()
        {
            List<Product> allProducts = new List<Product>();
            string[] productNames = new string[getAllProducts().Count];
            int productListCounter = 0;
            foreach (XmlNode item in getAllProducts())
            {
                Product p = new Product { productname = item.SelectSingleNode("name").InnerText + "", piecesTotal = Int32.Parse(item.SelectSingleNode("pieces").InnerText), price = Double.Parse(item.SelectSingleNode("price").InnerText),fsk= Int32.Parse(item.SelectSingleNode("fsk").InnerText) };
                allProducts.Add(p);
                productNames[productListCounter] = p.productname;
                productListCounter++;
            }
            return allProducts;
        }
    }
}
