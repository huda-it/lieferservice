﻿using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace KolmadoApp
{
    public static class HelpingFunctions
    {
        public static String convertPictureUrl(string pictureUrl)
        {
            String result = pictureUrl.ToLower().Replace("ä", "ae");
            result = result.Replace(" ", "");
            result = result.Replace("ö", "oe");
            result = result.Replace("ü", "ue");
            return result;
        }
        public static async System.Threading.Tasks.Task savePropertiesAsync(string propertyName="shoppingCart")
        {
            // Make sure you set the application name before doing any inserts or gets
            int counter = 0;
            removeCrossSettings();
             foreach (var item in (List<Product>)App.Current.Properties["shoppingCart"])
             {
                 string newName = propertyName + counter;
                 CrossSettings.Current.AddOrUpdateValue(newName,item.productname);
                 CrossSettings.Current.AddOrUpdateValue(newName+"Pieces", item.pieces);
                counter++;
             }
        }
        public static void removeCrossSettings(string propertyName = "shoppingCart")
        {
            int counter = 0;
            while (CrossSettings.Current.Contains(propertyName + counter))
            {
                CrossSettings.Current.Remove(propertyName+counter);
                counter++;
            }
        }
        public static List<Product> getProductsFromProperties(string propertyName= "shoppingCart")
        {
            int counter = 0;
            List<Product> result = new List<Product>();
            while (CrossSettings.Current.Contains(propertyName+counter))
            {
                String productname=""+CrossSettings.Current.GetValueOrDefault(propertyName + counter,"");
                int pieces =CrossSettings.Current.GetValueOrDefault(propertyName + counter + "Pieces", 1);
                Product productToAdd=null;
                foreach (var item in GlobalVars.listOfAllProducts)
                {
                    if (item.productname.Equals(productname)) productToAdd=item;
                }
                if(productToAdd!=null)
                {
                    productToAdd.pieces = pieces;
                    result.Add(productToAdd);
                }
                counter++;
            }
            return result;
        }
    }
}
