﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;

namespace KolmadoApp
{
    public class HttpRequests
    {
        public static void b()
        {
        }

        public static bool changeXmlFilePhpRequest(string host, string xmlFileName, string xmlText)
        {
            //XXDArum kümmern das string getrennt wird und einzeln geschickt wird
            int xmlTextCounter = 0;
            int xmlTextLength = 400;
            Boolean lastRound = false;
            using (WebClient client = new WebClient())
            {
                while (lastRound == false)
                {
                    if (xmlTextCounter + xmlTextLength > xmlText.Length)
                    {
                        xmlTextLength = xmlText.Length - xmlTextCounter;
                        lastRound = true;
                    }
                    string urlAddress = host + "/changeXml.php" + "?textToChange=" + xmlText.Substring(xmlTextCounter, xmlTextLength) + "&filename=" + xmlFileName;
                    // this string contains the webpage's source
                    string pagesource = client.DownloadString(urlAddress);
                    xmlTextCounter += 400;
                }
            }
            return true;
        }
        public static bool changeXmlFilePhpRequest2(string host, string xmlFileName, string xmlText)
        {
            //XXDArum kümmern das string getrennt wird und einzeln geschickt wird
            int xmlTextCounter = 0;
            int xmlTextLength = 400;
            Boolean lastRound = false;
            using (WebClient client = new WebClient())
            {
                Uri uri = new Uri(host + "/changeXml.php");
                NameValueCollection parameters = new NameValueCollection();

                parameters.Add("textToChange", xmlText);
                parameters.Add("filename", xmlFileName);

                var response = client.UploadValues(uri, "POST", parameters);
                string json = Encoding.UTF8.GetString(response);
               // string contentTest = JsonConvert.DeserializeObject<List<Contents>>(json);
                //txt.Text = mContents[0].success.ToString();
            }
            return true;
        }
        
        public static String phpRequestXml(string url, string xmlTagToGetValueFrom)
        {
            try
            {
                String startString = "";
                if (xmlTagToGetValueFrom.Contains("<"))
                {
                    startString = xmlTagToGetValueFrom;
                }
                else
                {
                    startString = "<" + xmlTagToGetValueFrom + ">";
                }
                String endString = startString.Replace("<", "</");
                using (WebClient client = new WebClient())
                {
                    string pagesource = client.DownloadString(url);
                    return StringMethods.ValueBetweenStringsInString(pagesource, startString, endString);
                }
            }
            catch (Exception e)
            {
                return "";
            };
        }

        public static String phpGetValueFromRequest(string url)
        {
            using (WebClient client = new WebClient())
            {
                string pagesource = client.DownloadString(url);
                return pagesource;
            }
        }

        public static String getOpenDate(string host)
        {
            var webRequest = WebRequest.Create(host + "/getOpenDate");
            DateTime beginningDate;
            DateTime endingDate;
            using (var response = webRequest.GetResponse())
            using (var content = response.GetResponseStream())
            using (var reader = new StreamReader(content))
            {
                string[] datesFromContent = new string[2];
                var strContent = reader.ReadToEnd();
                datesFromContent = strContent.Split('-');
                String x = datesFromContent[0];
                DateTime currentDate = DateTime.Now;
                beginningDate = DateTime.ParseExact(datesFromContent[0].Replace(" ", ""), "dd.MM.yyyyHH:mm", CultureInfo.CurrentCulture);
                endingDate = DateTime.ParseExact(datesFromContent[1].Replace(" ", ""), "dd.MM.yyyyHH:mm", CultureInfo.CurrentCulture);
                if ((currentDate.CompareTo(beginningDate) > 0 && (currentDate.CompareTo(endingDate)) < 0))
                {
                    return "";
                }
            }

            string result = "Geliefert wird erst wieder vom " + beginningDate.Day + "." + beginningDate.Month + " um " + beginningDate.Hour;
            if (beginningDate.Minute != 0) result += ":" + beginningDate.Minute;
            result += " Uhr";
            result += " bis zum " + endingDate.Day + "." + endingDate.Month + " um " + endingDate.Hour;
            if (endingDate.Minute != 0) result += ":" + endingDate.Minute;
            result += " Uhr";
            return result;
        }
    }
}
