﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KolmadoApp
{
    public static class LocationMethods
    {
        private static double laakirchenCoordinateX = 47.9909315;
        private static double laakirchenCoordinateY = 13.7984647;
        private static String GEOCODE_REQUEST_URL = "http://maps.googleapis.com/maps/api/geocode/xml?sensor=false&address=";
        public static double[] getLocationFromAdress(String address)
        {
            try{
            String status = "OVER_QUERY_LIMIT";
            String inputStreamToString = "";
            String addressXml = "";
            while (status.Equals("OVER_QUERY_LIMIT"))
            {
                addressXml = HttpRequests.phpGetValueFromRequest(GEOCODE_REQUEST_URL + address);
                status = StringMethods.ValueBetweenStringsInString(addressXml, "<status>", "</status>");
            }
            double langitude = double.Parse(StringMethods.ValueBetweenStringsInStringFirstOnly(addressXml, "<lat>", "</lat>").Replace(".", ","));
            double longitude = double.Parse(StringMethods.ValueBetweenStringsInStringFirstOnly(addressXml, "<lng>", "</lng>").Replace(".", ","));
            return new double[] { langitude, longitude };
            }
            catch(Exception e)
            {
                return new double[] { 0, 0 };
            }
        }
        public static bool checkIfLocationsIsIn15MetersRangeOfLaakirchen(string ort, string strasse, string hausnummer)
        {
            double[] coordinates = getLocationFromAdress(generateAddressFromEntries(ort, strasse, hausnummer));
            double x2 = coordinates[0];
            double y2 = coordinates[1];
            if (DistanceTo(laakirchenCoordinateX, laakirchenCoordinateY, x2, y2) < 15) return true;
            return false;
        }
        public static String generateAddressFromEntries(string ort, string strasse, string hausnummer)
        {
            return ort + "," + strasse + " " + hausnummer;
        }
        public static double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }

            return dist;
        }
    }
}
