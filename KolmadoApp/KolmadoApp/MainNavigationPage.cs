﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace KolmadoApp
{
    class MainNavigationPage : NavigationPage
    {
            public MainNavigationPage(string title)
            {
                SetBackButtonTitle(new ShoppingCartPage(), "Back");
            }
        }
}
