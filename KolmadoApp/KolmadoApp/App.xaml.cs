﻿using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace KolmadoApp
{
	public partial class App : Application
    {
        public static Object androidBundle;
        public App ()
		{
            loadCategoryImages();
            InitializeComponent();
            MainPage =new NavigationPage( new MainPage(true));
		}
        public static PaypalPreferences PaypalPreferences { get; private set; }

        public static void Init(PaypalPreferences paypalPreferencesImpl)
        {
            App.PaypalPreferences = paypalPreferencesImpl;
        }
        public static async System.Threading.Tasks.Task savePropertiesPermanentlyAsync()
        {
            await Application.Current.SavePropertiesAsync();
        }
        public static NavigationPage GetGlobalNavigationPage(Page page,string title)
        {
            NavigationPage result = new NavigationPage(page)
            {
                
            };
            result.Icon = "back.png";
            result.Title = title;
            return result;
        }
        public static void loadCategoryImages()
        {
            string host = GlobalVars.host;
            string allProductsPath = GlobalVars.productsXml;
            ProductXmlMethods pxm = new ProductXmlMethods();
            pxm.proudctDoc.Load(host + allProductsPath);
            GlobalVars.allCategoriesGlobal = pxm.getAllCategories(host);

        }
        public static Page GetMainPage()
        {
            return new ContentPage
            {
                Content = new Label
                {
                    Text = "Hello, Forms!",
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                },
            };
        }

        

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
    }
}
