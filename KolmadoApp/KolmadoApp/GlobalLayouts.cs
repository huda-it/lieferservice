﻿/*using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace KolmadoApp
{
    public class GlobalLayouts 
    {
        public static string title { get; set; }
        static Label lbl_header = new Label
        {
            Text = title,
            FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HorizontalOptions = LayoutOptions.Center
        };

        static Label lbl_shoppingCart = new Label
        {
            // Text = App.Current.Properties["shoppingCart"].products.size(),
            FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
            HorizontalOptions = LayoutOptions.EndAndExpand
        };
        //Bild zentrieren
        public static Image img_shoppingCart = new Image
        {
            Source = "shoppingCart.png",
            HorizontalOptions = LayoutOptions.EndAndExpand
        };
        
        public static StackLayout headerLayout
        {
            get
            {
                lbl_header.Text = title;
                return new StackLayout
                {
                    Padding = new Thickness(20, 5, 20, 5),
                    BackgroundColor = Color.WhiteSmoke,
                    VerticalOptions=LayoutOptions.StartAndExpand,
                    Children ={
                        new StackLayout{
                            Orientation = StackOrientation.Horizontal,
                            Margin=new Thickness(0,15,0,0),
                            Children={
                                new Image { Source = "back.png"},
                                lbl_header,
                                img_shoppingCart
                                // lbl_shoppingCart
                            }
                        }
                }
                };
            }
        }
        public static Image homeIcon {
            get {
                return new Image
                {
                    Source = "home.png",
                    HorizontalOptions = LayoutOptions.CenterAndExpand
                };
                }
        }

        public static StackLayout footerLayout {
            get {
                return new StackLayout
                {
                    Padding = new Thickness(20, 5, 20, 5),
                    Orientation = StackOrientation.Horizontal,
                    VerticalOptions = LayoutOptions.EndAndExpand,
                    BackgroundColor = Color.WhiteSmoke,
                    Children ={
                    homeIcon
                }
                };
            }
        }
    }
}*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace KolmadoApp
{
    public class GlobalLayouts
    {
        public static string title { get; set; }
        static Label lbl_header = new Label
        {
            Text = title,
            FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
            HorizontalOptions = LayoutOptions.Center
        };

        static Label lbl_shoppingCart = new Label
        {
            // Text = App.Current.Properties["shoppingCart"].products.size(),
            FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
            HorizontalOptions = LayoutOptions.EndAndExpand
        };
        //Bild zentrieren
        public static Image img_shoppingCart = new Image
        {
            Source = "shoppingCart.png",
            HorizontalOptions = LayoutOptions.EndAndExpand
        };

        public static StackLayout headerLayout
        {
            get
            {
                lbl_header.Text = title;
                return new StackLayout
                {
                    Padding = new Thickness(20, 5, 20, 5),
                    BackgroundColor = Color.WhiteSmoke,
                    VerticalOptions = LayoutOptions.StartAndExpand,
                    Children ={
                        new StackLayout{
                            Orientation = StackOrientation.Horizontal,
                            Margin=new Thickness(0,15,0,0),
                            Children={
                                new Image { Source = "back.png"},
                                lbl_header,
                                img_shoppingCart
                                // lbl_shoppingCart
                            }
                        }
                }
                };
            }
        }
        public static Image homeIcon { get {
                return new Image
                {
                    Source = "home.png",
                    HorizontalOptions = LayoutOptions.CenterAndExpand
                }; } }

        public static StackLayout footerLayout = new StackLayout
        {
            Padding = new Thickness(20, 5, 20, 5),
            Orientation = StackOrientation.Horizontal,
            VerticalOptions = LayoutOptions.EndAndExpand,
            BackgroundColor = Color.WhiteSmoke,
            Children ={
                    homeIcon
                }
        };
        public static BoxView border { get { return new BoxView { BackgroundColor = Color.Gray, HeightRequest = 1, VerticalOptions = LayoutOptions.Start, HorizontalOptions = LayoutOptions.FillAndExpand }; } }
        public static BoxView borderVertical { get { return new BoxView { BackgroundColor = Color.Gray, HeightRequest = 1, VerticalOptions = LayoutOptions.FillAndExpand, HorizontalOptions = LayoutOptions.Start }; } }
        public static void InitializeFooterLayout()
        {
            footerLayout = new StackLayout
            {
                Padding = new Thickness(20, 5, 20, 5),
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.EndAndExpand,
                BackgroundColor = Color.WhiteSmoke,
                Children ={
                    homeIcon
                }
            };
        }
    }
}