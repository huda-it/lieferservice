﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace KolmadoApp.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImageButton : StackLayout
    {
        public static readonly BindableProperty ShowedImageProperty = BindableProperty.Create(
  nameof(ShowedImage), typeof(string), typeof(ImageButton));
        public static readonly BindableProperty ShowedTextProperty = BindableProperty.Create(
  nameof(ShowedText), typeof(string), typeof(ImageButton));
        public ImageButton()
        {
            BindingContext = this;
            InitializeComponent();
        }
        public string ShowedImage
        {
            get { return (string)GetValue(ShowedImageProperty); }
            set { SetValue(ShowedImageProperty, value); }
        }
        public string ShowedText
        {
            get { return (string)GetValue(ShowedTextProperty); }
            set { SetValue(ShowedTextProperty, value); }
        }

    }
}