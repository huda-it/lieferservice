﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace KolmadoApp.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomCircleButton : Frame
    {
        public CustomCircleButton()
        {
            InitializeComponent();
        }
    }
}