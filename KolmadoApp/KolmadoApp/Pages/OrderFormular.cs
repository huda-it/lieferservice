﻿using KolmadoApp.Controls;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace KolmadoApp
{
    public class OrderFormular : ContentPage
    {
        public OrderFormular()
        {
            #region footer
            
            ImageButton homeIcon = new ImageButton { ShowedImage = "home.png", ShowedText = "Home" };
            ImageButton ordersIcon = new ImageButton { ShowedImage = "orders.png", ShowedText = "Orders", HeightRequest = 35 };
            ImageButton contactIcon = new ImageButton { ShowedImage = "contact.png", ShowedText = "Contact", HeightRequest = 35 };
            homeIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new MainPage());
                },
                NumberOfTapsRequired = 1
            });
            ordersIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    List<Product> orderProducts = HelpingFunctions.getProductsFromProperties("OrderProducts");
                    App.Current.Properties["OrderProducts"] = orderProducts;
                    if (orderProducts.Count > 0)
                    {
                        Navigation.PushAsync(new DeliveryPage());
                    }
                    else
                    {
                        DisplayAlert("Keine Bestellungen", "Leider haben Sie noch keine Bestellungen getätigt.", "OK");
                    }
                },
                NumberOfTapsRequired = 1
            });
            contactIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new ContactPage());
                },
                NumberOfTapsRequired = 1
            });

            var footerGrid = new Grid();
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.Children.Add(homeIcon, 0, 0);
            footerGrid.Children.Add(ordersIcon, 1, 0);
            footerGrid.Children.Add(contactIcon, 2, 0);
            #endregion
            ToolbarItem shoppingCartCounterTI = new ToolbarItem
            {
                Command = new Command(async () =>
await Navigation.PushAsync(new ShoppingCartPage())),
                Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + ""
            };
            var shoppingCartIconTI = new ToolbarItem
            {
                Command = new Command(async () =>
                await Navigation.PushAsync(new ShoppingCartPage())),
                Icon = "shoppingCart.png",
                Name = "Einkaufswagen"
            };
            ToolbarItems.Add(shoppingCartIconTI);
            ToolbarItems.Add(shoppingCartCounterTI);
            Title = "Bestellformular";
            NavigationPage.SetBackButtonTitle(this, "Bestellformular");
            StackLayout mainLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions=LayoutOptions.FillAndExpand,
                Padding = new Thickness(20, 20, 20, 20)
            };
            Label l_persoenliche = new Label() { Text = "Persönliche Daten", FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) };
            Label l_datumDerLieferung = new Label { Text = "Gewünschte Lieferzeit", FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)), Margin = new Thickness(0, 20, 0, 0) };
            Label l_ortDerLieferung = new Label { Text = "Lieferort", FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)), Margin = new Thickness(0, 20, 0, 0) };
            var e_name = new Entry {Text= CrossSettings.Current.GetValueOrDefault("account_name","Name...") };
            e_name.Focused += (sender, args) => { if (e_name.Text.Equals("Name...")) e_name.Text = ""; };
            var e_ort = new Entry { Text = CrossSettings.Current.GetValueOrDefault("account_ort", "Ort...") };
            e_ort.Focused += (sender, args) => { if (e_ort.Text.Equals("Ort...")) e_ort.Text = ""; };
            var e_street = new Entry { Text = CrossSettings.Current.GetValueOrDefault("account_strasse", "Straße...") };
            e_street.Focused += (sender, args) => { if (e_street.Text.Equals("Straße...")) e_street.Text = ""; };
            var e_homeNumber = new Entry { Text = CrossSettings.Current.GetValueOrDefault("account_hausnummer", "Hausnummer..."), Keyboard = Keyboard.Numeric };
            e_homeNumber.Focused += (sender, args) => { if (e_homeNumber.Text.Equals("Hausnummer...")) e_homeNumber.Text = ""; };
            var e_telefonNumber = new Entry { Text = CrossSettings.Current.GetValueOrDefault("account_telefonnummer", "Telefonnummer...(optional)"), Keyboard = Keyboard.Telephone };
            e_telefonNumber.Focused += (sender, args) => { if (e_telefonNumber.Text.Equals("Telefonnummer...(optional)")) e_telefonNumber.Text = ""; };
            var e_age = new Entry { Text = CrossSettings.Current.GetValueOrDefault("account_alter", "Alter..."), Keyboard = Keyboard.Numeric };
            e_age.Focused += (sender, args) => { if (e_age.Text.Equals("Alter...")) e_age.Text = ""; };
            var e_email = new Entry { Text = CrossSettings.Current.GetValueOrDefault("account_email", "Email..."), Keyboard = Keyboard.Email };
            e_email.Focused += (sender, args) => { if (e_email.Text.Equals("Email...")) e_email.Text = ""; };
            DatePicker datePicker = new DatePicker
            {
                Format = "D"
            };
            TimePicker timePicker = new TimePicker() { Time = new TimeSpan(17, 0, 0) };
            Button btn_buying = new Button
            {
                Text = "Zahlen",
                BorderColor = Color.Gray,
                BackgroundColor = Color.Red,
                TextColor = Color.White,
                BorderWidth = 1,
                Font = Font.SystemFontOfSize(NamedSize.Medium).WithAttributes(FontAttributes.Bold)
            };
            /* StackLayout row1 = new StackLayout
             {
                 Orientation = StackOrientation.Horizontal,
                 Children =
                 {
                     new Label
                     {
                         Text="Ort:"
                     },
                     new Label
                     {
                         Text="Ort:"
                     },
                     MyEntry
                 }
             };*/
            mainLayout.Children.Add(l_persoenliche);
            mainLayout.Children.Add(e_name);
            mainLayout.Children.Add(e_age);
            //mainLayout.Children.Add(e_telefonNumber);
            //mainLayout.Children.Add(e_email);
            mainLayout.Children.Add(l_ortDerLieferung);
            mainLayout.Children.Add(e_ort);
            mainLayout.Children.Add(e_street);
            mainLayout.Children.Add(e_homeNumber);
            //mainLayout.Children.Add(l_datumDerLieferung);
            //mainLayout.Children.Add(datePicker);
           // mainLayout.Children.Add(timePicker);
            mainLayout.Children.Add(btn_buying);
            btn_buying.Clicked += async (sender, args) =>
            {

                String openDateMessage = HttpRequests.getOpenDate(GlobalVars.host);
                if (openDateMessage.Equals(""))
                {
                    if (checkIfEntriesAreFilled(e_age, e_street, e_ort, e_name, e_homeNumber))
                    {
                        if (ProductListMethods.CheckIfAgeIsAllowed(int.Parse(e_age.Text), (List<Product>)App.Current.Properties["shoppingCart"]))
                        {
                            //XX Noch bessere Nachricht
                            //Mir Anzeige welche Daten man eingebeben hat Lieferzeit etc
                            //App.Current.Properties["OrderProducts"] = App.Current.Properties["shoppingCart"];
                            Person p = new Person { id = "", ort = e_ort.Text, straße = e_street.Text, hausnummer = e_homeNumber.Text, nachname = e_name.Text, alter = int.Parse(e_age.Text) };
                           /* if (e_telefonNumber.Text.Equals("Telefonnummer...") == false)
                            {
                                p.tel = e_telefonNumber.Text + "";
                            }*/
                            //p.lieferzeit = datePicker.Date.Day + "." + datePicker.Date.Month + "." + datePicker.Date.Year + " - " + timePicker.Time.ToString();
                            p.productsOfPerson = (List<Product>)App.Current.Properties["shoppingCart"];
                            bool personAdded = p.addPersonToPersons(p);
                            if (personAdded)
                            {

                                int resultOfPaypalRequest = -99;
                                resultOfPaypalRequest = App.PaypalPreferences.PaypalPage(ProductListMethods.GetPaypalItemsFromProducts((List<Product>)App.Current.Properties["shoppingCart"]));
                            }
                            else
                            {
                                await DisplayAlert("Abgebrochen", "Leider gab es einen Fehler bei der Datenübertragung, bitte überprüfen Sie die Internetverbindung.", "Okay");
                            }
                            //XX Productnamen, Preise und Stückanzahl einbauen
                            
                        }
                        else
                        {
                           await DisplayAlert("Alterbeschränkung", "Eines Ihrer Produkte ist für ihr Alter nicht zugelassen.", "OK");
                        }
                    }
                    else
                    {
                        await DisplayAlert("Felder ausfüllen", "Es müssen alle Felder ausgefüllt werden!", "OK");
                    }
                }
                else
                {
                   await DisplayAlert("Außerhalb der Lieferzeit", openDateMessage, "OK");
                }
            };
            //Present Alert
            Content =new StackLayout
                {
                    Children = {

                        new ScrollView{Content=mainLayout },
                        new Grid { VerticalOptions = LayoutOptions.FillAndExpand },
                    footerGrid
                }
            };
        }
        public Boolean checkIfEntriesAreFilled(Entry e_age, Entry e_street, Entry e_city, Entry e_name, Entry e_homeNr)
        {
            Boolean result = true;
            if (e_age.Text.Equals("Alter...") || e_street.Text.Equals("Straße...") || e_city.Text.Equals("Ort...")  || e_name.Text.Equals("Name...") || e_homeNr.Text.Equals("Hausnummer...")) result = false;
            if (e_age.Text.Equals("") || e_street.Text.Equals("") || e_city.Text.Equals("") ||  e_homeNr.Text.Equals("") || e_name.Text.Equals("")) result = false;
            return result;
        }
        /* public Boolean checkIfEntriesAreFilled(Entry e_age, Entry e_street, Entry e_city, Entry e_telNr, Entry e_name, Entry e_homeNr)
         {
             Boolean result = true;
             if (e_age.Text.Equals("Alter...") || e_street.Text.Equals("Straße...") || e_city.Text.Equals("Ort...") || e_telNr.Text.Equals("Telefonnummer...") || e_name.Text.Equals("Name...") || e_homeNr.Text.Equals("Hausnummer...")) result = false;
             if (e_age.Text.Equals("") || e_street.Text.Equals("") || e_city.Text.Equals("") || e_telNr.Text.Equals("") || e_homeNr.Text.Equals("") || e_name.Text.Equals("")) result = false;
             return result;
         }*/
    }
}