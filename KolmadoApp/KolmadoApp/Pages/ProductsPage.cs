﻿using KolmadoApp.Controls;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Flex.Controls;

namespace KolmadoApp
{
    public class ProductsPage : ContentPage
    {
        //XX Einzelne Produkte noch anschauen!!
        public List<Product> productsPage_productsList { get; set; }
        Product[] currentProductCreated = new Product[2];
        //ToolbarItems[1] wegen dem aktuallisieren weil shoppingCartCounter wenn man anspricht nicht reagiert nach backbuttonpressed
        public ProductsPage(CategoriesAndProducts categoriesAndProducts)
        {
            mainProductsPage(categoriesAndProducts);
        }
        public void mainProductsPage(CategoriesAndProducts categoriesAndProducts, bool refreshing = false)
        {
            #region footer
            BackgroundColor = Color.AliceBlue;
            ImageButton homeIcon = new ImageButton { ShowedImage = "home.png", ShowedText = "Home" };
            ImageButton ordersIcon = new ImageButton { ShowedImage = "orders.png", ShowedText = "Orders", HeightRequest = 35 };
            ImageButton contactIcon = new ImageButton { ShowedImage = "contact.png", ShowedText = "Contact", HeightRequest = 35 };
            homeIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new MainPage());
                },
                NumberOfTapsRequired = 1
            });
            ordersIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    List<Product> orderProducts = HelpingFunctions.getProductsFromProperties("OrderProducts");
                    App.Current.Properties["OrderProducts"] = orderProducts;
                    if (orderProducts.Count > 0)
                    {
                        Navigation.PushAsync(new DeliveryPage());
                    }
                    else
                    {
                        DisplayAlert("Keine Bestellungen", "Leider haben Sie noch keine Bestellungen getätigt.", "OK");
                    }
                },
                NumberOfTapsRequired = 1
            });
            contactIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new ContactPage());
                },
                NumberOfTapsRequired = 1
            });

            var footerGrid = new Grid();
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.Children.Add(homeIcon, 0, 0);
            footerGrid.Children.Add(ordersIcon, 1, 0);
            footerGrid.Children.Add(contactIcon, 2, 0);
            #endregion

            Title = categoriesAndProducts.categoryName;
            StackLayout ges;
            NavigationPage.SetBackButtonTitle(this, "Produkte");
            ToolbarItem shoppingCartCounterTI = new ToolbarItem
            {
                Command = new Command(async () =>
await Navigation.PushAsync(new ShoppingCartPage())),
                Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + ""
            };
            var shoppingCartIconTI = new ToolbarItem
            {
                Command = new Command(async () =>
                await Navigation.PushAsync(new ShoppingCartPage())),
                Icon = "shoppingCart.png",
                Name = "Einkaufswagen"
            };
            if (refreshing == false) ToolbarItems.Add(shoppingCartIconTI);
            if (refreshing == false) ToolbarItems.Add(shoppingCartCounterTI);
            ToolbarItems.Last().Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + "";
            GlobalLayouts.title = categoriesAndProducts.categoryName;
            //GlobalLayouts.title = "JJJ";
            //GlobalLayouts.refreshHeader();
            //XX NOTIZEN MAXIMALE STÜCKANZAHL ALS TAG ODER DERGLEICHEN HINZUFÜGEN UM DANN EINE KONTROLLE ZU HABEN


            StackLayout mainProductLayout = new StackLayout
            {
                Spacing = 10,
                Orientation = StackOrientation.Vertical,
                VerticalOptions=LayoutOptions.FillAndExpand
            };



            this.productsPage_productsList = categoriesAndProducts.products;


            //Images
            if (productsPage_productsList.Count != 1 || true)
            {
                int z = 0;
                StackLayout row1Layout;
                StackLayout row2Layout;
                Grid row3Layout;
                StackLayout row4Layout;
                for (int i = 0; i < productsPage_productsList.Count; i = i + 2)
                {
                    this.currentProductCreated[0] = productsPage_productsList[i];
                    if (i + 2 <= productsPage_productsList.Count)
                    {
                        this.currentProductCreated[1] = productsPage_productsList[i + 1];
                        row1Layout = setRow1(productsPage_productsList[i],productsPage_productsList[i + 1]);
                        //buttons minus, Labels price, buttons plus
                        row2Layout = setRow2();
                        //product name
                        row3Layout = setRow3(productsPage_productsList[i].productname, productsPage_productsList[i + 1].productname);
                        //price
                        row4Layout = setRow4(productsPage_productsList[i].price + "", productsPage_productsList[i + 1].price + "");
                    }
                    else
                    {
                        row1Layout = setRow1(productsPage_productsList[i]);
                        //buttons minus, Labels price, buttons plus
                        row2Layout = setRow2(true);
                        //product name
                        row3Layout = setRow3(productsPage_productsList[i].productname, "");
                        //price
                        row4Layout = setRow4(productsPage_productsList[i].price + "", "");
                    }
                    mainProductLayout.Children.Add(row1Layout);
                    mainProductLayout.Children.Add(row3Layout);
                    mainProductLayout.Children.Add(row2Layout);
                    mainProductLayout.Children.Add(row4Layout);
                }
                //Stelle Page zusammen
                ScrollView mainScrollView = new ScrollView
                {
                    Content = mainProductLayout

                };
                var tapGestureRecognizer = new TapGestureRecognizer();
                tapGestureRecognizer.Tapped += (s, e) => { ChangePageShoppingCart(); };
                GlobalLayouts.img_shoppingCart.GestureRecognizers.Add(tapGestureRecognizer);
                ges = new StackLayout
                {
                    Children =
{
mainScrollView,
}
                };
            }
            else
            {

                //Nur ein Produkt

                ges = new StackLayout
                {
                    Children =
{
setOnlyOneProduct(),
}
                };
            }
            ges.Children.Add(new Grid { VerticalOptions = LayoutOptions.FillAndExpand });
            ges.Children.Add(footerGrid);
            Content = ges;

            //Zusammenstellen Ende


        }
        public async Task ChangePageShoppingCart()
        {
            //NavigationPage.SetBackButtonTitle(this, "Kategorien");
            await Navigation.PushAsync(new ShoppingCartPage());
        }
        protected override void OnAppearing()
        {
            if (App.Current.Properties.ContainsKey("updateWindow"))
            {
                if ((bool)App.Current.Properties["updateWindow"])
                {
                    CrossSettings.Current.AddOrUpdateValue("shoppingCartCounter", 0);
                    // ToolbarItems.Last().Text = "0";
                    mainProductsPage(new CategoriesAndProducts { categoryName = "Bier", products = productsPage_productsList }, true);
                    App.Current.Properties["updateWindow"] = false;
                }
            }
        }
        public StackLayout setRow2(bool onlyOne = false)
        {

            StackLayout row2Layout = new StackLayout();
            row2Layout.Orientation = StackOrientation.Horizontal;
            if (onlyOne == false)
            {
                StackLayout buttonsLayout = setButtonsLayout(true);
                row2Layout.Children.Add(buttonsLayout);
                StackLayout buttonsLayout2 = setButtonsLayout();
                row2Layout.Children.Add(buttonsLayout2);
            }
            else
            {
                StackLayout buttonsLayout = setButtonsLayout(true, true);
                row2Layout.Children.Add(buttonsLayout);

            }
            return row2Layout;
        }
        public StackLayout setButtonsLayout(bool first = false, bool onlyOne = false)
        {
            Product productToAddOrDelete;

            if (first)
            {
                productToAddOrDelete = this.currentProductCreated[0];
            }
            else
            {
                productToAddOrDelete = this.currentProductCreated[1];
            }

            List<Product> products = (List<Product>)App.Current.Properties["shoppingCart"];

            StackLayout buttonsLayout = new StackLayout();
            Label lbl_productPieces = new Label { Text = "0", HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.Center, HorizontalTextAlignment = TextAlignment.Center };
            if (ProductListMethods.IsProductInList(productToAddOrDelete.productname, products))
            {
                lbl_productPieces.Text = "" + products[ProductListMethods.FindProductsIndexInList(productToAddOrDelete.productname, products)].pieces;
            }
            //Buttons
            FlexButton btn_minus = new FlexButton
            {
                Text = "-",
                BackgroundColor = Color.Gray,
                // Font = Font.SystemFontOfSize(NamedSize.Medium),
                // BorderWidth = 1,
                //BackgroundColor = Color.Gray,
                //TextColor = Color.White,
                CornerRadius = 15,
                WidthRequest = 30,
                HeightRequest = 30,
                HorizontalOptions = LayoutOptions.Center,
            };
            btn_minus.Clicked += delegate
            {

                if (lbl_productPieces.Text.Equals("1"))
                {
                    var itemToRemove = products.Single(r => r.productname.Equals(productToAddOrDelete.productname));
                    products.Remove(itemToRemove);
                    CrossSettings.Current.AddOrUpdateValue("shoppingCartCounter", CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) - 1);
                    ToolbarItems[1].Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + "";
                    lbl_productPieces.Text = "0";
                }
                else if (lbl_productPieces.Text.Equals("0") == false)
                {
                    CrossSettings.Current.AddOrUpdateValue("shoppingCartCounter", CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) - 1);
                    ToolbarItems[1].Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + "";
                    products[ProductListMethods.FindProductsIndexInList(productToAddOrDelete.productname, products)].pieces--;
                    lbl_productPieces.Text = "" + products[ProductListMethods.FindProductsIndexInList(productToAddOrDelete.productname, products)].pieces;
                }
            };
            
            FlexButton btn_plus = new FlexButton
            {
                Text = "+",
                
               // Font = Font.SystemFontOfSize(NamedSize.Medium),
                BackgroundColor = Color.Gray,
                CornerRadius=15,
               // TextColor=Color.White,
                //TextColor = Color.White,
                WidthRequest = 30,
                HeightRequest = 30,
            };
            if (onlyOne)
            {
                btn_minus.HorizontalOptions = LayoutOptions.EndAndExpand;
                btn_plus.HorizontalOptions = LayoutOptions.StartAndExpand;
            }
            btn_plus.Clicked += delegate
            {
                // if (Int32.Parse(lbl_productPieces.Text) == 0) products[ProductListMethods.FindProductsIndexInList(productToAddOrDelete.productname, products)].pieces = 0;
                if (productToAddOrDelete.piecesTotal > productToAddOrDelete.pieces)
                {
                    CrossSettings.Current.AddOrUpdateValue("shoppingCartCounter", CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + 1);
                    ToolbarItems[1].Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + "";
                    if (ProductListMethods.IsProductInList(productToAddOrDelete.productname, products) == false)
                    {
                        double pfand = productToAddOrDelete.pfand;
                        products.Add(productToAddOrDelete);
                        //Keine Ahnung warum ich die nächste Zeile brauche
                        products[ProductListMethods.FindProductsIndexInList(productToAddOrDelete.productname, products)].pieces = 1;
                    }
                    else
                    {
                        products[ProductListMethods.FindProductsIndexInList(productToAddOrDelete.productname, products)].pieces++;
                    }
                    lbl_productPieces.Text = products[ProductListMethods.FindProductsIndexInList(productToAddOrDelete.productname, products)].pieces + "";
                }
                else
                {
                    DisplayAlert("Nicht auf Lager", "Wir haben derzeit leider nur noch " + productToAddOrDelete.piecesTotal + " Stück auf Lager", "OK");
                }
                App.Current.SavePropertiesAsync();
            };
            buttonsLayout.Children.Add(btn_minus);
            buttonsLayout.Children.Add(lbl_productPieces);
            buttonsLayout.Children.Add(btn_plus);
            buttonsLayout.HorizontalOptions = LayoutOptions.FillAndExpand;
            buttonsLayout.Orientation = StackOrientation.Horizontal;
            buttonsLayout.Padding = new Thickness(10, 10);
            return buttonsLayout;
        }
        public StackLayout setRow1(Product p1,Product p2=null)
        {

            StackLayout productImageLayout = new StackLayout();
            productImageLayout.Orientation = StackOrientation.Horizontal;

            productImageLayout.Children.Add(generateImageForProductRow(p1));
            if (p2!=null) productImageLayout.Children.Add(generateImageForProductRow(p2));
            return productImageLayout;
        }
        public Image generateImageForProductRow(Product product)
        {
            Image productImage = new Image { Source = ImageSource.FromUri(new Uri(product.pictureUrl)), HorizontalOptions = LayoutOptions.FillAndExpand };
            Image secondImage = new Image { Source = ImageSource.FromUri(new Uri(product.pictureUrl)), HorizontalOptions = LayoutOptions.FillAndExpand };
            productImage.GestureRecognizers.Add((new TapGestureRecognizer
            {
                Command = new Command(async (o) =>
                {
                    await Navigation.PushAsync(new ProductDetailPage(secondImage,product));
                }),
                CommandParameter = secondImage,
            }));
            //productImage.Scale = 0.75;
            return productImage;
        }
        
        
        public Grid setRow3(String productName1, String productName2)
        {

            var productNameLayout = new Grid();
            productNameLayout.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            if(!productName2.Equals("")) productNameLayout.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            Label lbl_productName1 = new Label { Text = productName1, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)), FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalTextAlignment = TextAlignment.Center };
            productNameLayout.Children.Add(lbl_productName1, 0, 0);
            if (productName2.Equals("") == false)
            {
                Label lbl_productName2 = new Label { Text = productName2, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)), FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center };
                productNameLayout.Children.Add(lbl_productName2, 1, 0);
            }
            return productNameLayout;
        }

        public StackLayout setRow4(String price1, String price2)
        {

            StackLayout priceLayout = new StackLayout
            {
                Margin = new Thickness(0, 0, 0, 50),
                Orientation = StackOrientation.Horizontal
            };
            Label lbl_price1 = new Label { Text = String.Format("{0:0.00}", Double.Parse(price1)) + " Euro", FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)), HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.Center, TextColor = Color.Green, HorizontalTextAlignment = TextAlignment.Center };
            priceLayout.Children.Add(lbl_price1);
            if (price2.Equals("") == false)
            {
                Label lbl_price2 = new Label { Text = String.Format("{0:0.00}", Double.Parse(price2)) + " Euro", FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)), HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.Center, TextColor = Color.Green, HorizontalTextAlignment = TextAlignment.Center };
                priceLayout.Children.Add(lbl_price2);
            }
            return priceLayout;
        }

        public StackLayout setOnlyOneProduct()
        {
            Product p = productsPage_productsList[0];
            Image productImage = new Image { Source = ImageSource.FromUri(new Uri(HelpingFunctions.convertPictureUrl(p.pictureUrl))), HorizontalOptions = LayoutOptions.FillAndExpand, };
            StackLayout buttonsLayout = setButtonsLayout(true);
            buttonsLayout.Padding = new Thickness(50, 50);

            Label lbl_productName = new Label { Text = p.productname, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand, VerticalTextAlignment = TextAlignment.Center, HorizontalTextAlignment = TextAlignment.Center };
            Label lbl_price = new Label { Text = String.Format("{0:0,00}", Double.Parse(p.price + "")) + " Euro", HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand, VerticalTextAlignment = TextAlignment.Center, HorizontalTextAlignment = TextAlignment.Center };
            StackLayout result = new StackLayout
            {
                Spacing = 20,
                Orientation = StackOrientation.Vertical,
                Margin = 100,
                Children =
{
productImage,
buttonsLayout,
lbl_productName,
lbl_price,
new Grid{VerticalOptions=LayoutOptions.FillAndExpand}
}
            };
            return result;
        }
        protected override void OnDisappearing()
        {
            HelpingFunctions.savePropertiesAsync();
        }

    }
}

