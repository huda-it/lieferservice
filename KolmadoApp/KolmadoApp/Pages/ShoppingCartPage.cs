﻿using KolmadoApp.Controls;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace KolmadoApp
{
    public class ShoppingCartPage : ContentPage
    {

        public ShoppingCartPage()
        {
            #region footer
            
            ImageButton homeIcon = new ImageButton { ShowedImage = "home.png", ShowedText = "Home" };
            ImageButton ordersIcon = new ImageButton { ShowedImage = "orders.png", ShowedText = "Orders", HeightRequest = 35 };
            ImageButton contactIcon = new ImageButton { ShowedImage = "contact.png", ShowedText = "Contact", HeightRequest = 35 };
            homeIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new MainPage());
                },
                NumberOfTapsRequired = 1
            });
            ordersIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    List<Product> orderProducts = HelpingFunctions.getProductsFromProperties("OrderProducts");
                    App.Current.Properties["OrderProducts"] = orderProducts;
                    if (orderProducts.Count > 0)
                    {
                        Navigation.PushAsync(new DeliveryPage());
                    }
                    else
                    {
                        DisplayAlert("Keine Bestellungen", "Leider haben Sie noch keine Bestellungen getätigt.", "OK");
                    }
                },
                NumberOfTapsRequired = 1
            });
            contactIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new ContactPage());
                },
                NumberOfTapsRequired = 1
            });

            var footerGrid = new Grid { VerticalOptions=LayoutOptions.End };
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.Children.Add(homeIcon, 0, 0);
            footerGrid.Children.Add(ordersIcon, 1, 0);
            footerGrid.Children.Add(contactIcon, 2, 0);
            #endregion
            ToolbarItem shoppingCartCounterTI = new ToolbarItem
            {
                Command = new Command(async () =>
await Navigation.PushAsync(new ShoppingCartPage())),
                Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + ""
            };
            var shoppingCartIconTI = new ToolbarItem
            {
                Command = new Command(async () =>
                await Navigation.PushAsync(new ShoppingCartPage())),
                Icon = "shoppingCart.png",
                Name = "Einkaufswagen"
            };
            //ToolbarItems.Add(shoppingCartIconTI);
            ToolbarItems.Add(shoppingCartCounterTI);

            Title = "Warenkorb";
            List<Product> shoppingCartProducts = (List<Product>)App.Current.Properties["shoppingCart"];
            NavigationPage.SetBackButtonTitle(this, "Warenkorb");
            Button btn_goToOrder = new Button
            {
                Text = "Zum Bestellformular",
                BorderColor = Color.Gray,
                BackgroundColor = Color.Red,
                TextColor = Color.White,
                BorderWidth = 1,
                Font = Font.SystemFontOfSize(NamedSize.Medium).WithAttributes(FontAttributes.Bold)
            };
            if (ProductListMethods.GetTotalPriceOfProducts(shoppingCartProducts) == 0)
            {
                btn_goToOrder.Text = "Keine Produkte im Warenkorb";
            }
            else
            {
                btn_goToOrder.Clicked += delegate
                {
                    changeOrderFormularPageAsync();
                };
            }
            var shoppingCartDelItem = new ToolbarItem
            {
                Command = new Command(async () =>
                {
                    App.Current.Properties["shoppingCart"] = new List<Product>();
                    CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0);
                    App.Current.Properties["updateWindow"] = true;
                    Content = new StackLayout
                    {
                        Children =
                    {
                        new Button
                            {
                Text = "Keine Produkte im Warenkorb",
                BorderColor = Color.Gray,
                BackgroundColor = Color.Red,
                TextColor = Color.White,
                BorderWidth = 1,
                Font = Font.SystemFontOfSize(NamedSize.Medium).WithAttributes(FontAttributes.Bold)},
                        new Grid{VerticalOptions=LayoutOptions.FillAndExpand,HorizontalOptions=LayoutOptions.FillAndExpand},
                    footerGrid
                    }
                    };
                }
                ),
                Icon = "shoppingCartDel.png",
                Name = "Leeren"
            };


            ToolbarItems.Add(shoppingCartDelItem);
            StackLayout mainProductLayout = new StackLayout
            {
                Spacing = 10,
                Orientation = StackOrientation.Vertical
            };

            ScrollView mainScrollView = new ScrollView
            {
                Content = mainProductLayout

            };
            foreach (var item in shoppingCartProducts)
            {
                StackLayout row = new StackLayout()
                {
                    Orientation = StackOrientation.Horizontal,
                    Padding = new Thickness(5, 20, 20, 20)
                };

                //ProductImage
                row.Children.Add(new Image { Source = item.pictureUrl, VerticalOptions = LayoutOptions.StartAndExpand,Scale=0.85 });
                //Name und Preis
                Label l_priceOfProduct;
                if(item.pfand>0){
                l_priceOfProduct = new Label
                {
                    Text = (item.price + item.pfand) + " EUR inkl. Pfand",
                        TextColor = Color.Green,
                    Font = Font.SystemFontOfSize(NamedSize.Medium)
                };
                }
                else{
                    l_priceOfProduct = new Label
                    {
                        Text = (item.price) + " EUR",
                        TextColor = Color.Green,
                        Font = Font.SystemFontOfSize(NamedSize.Medium)
                    };
                }
                StackLayout nameLayout = new StackLayout
                {
                    Orientation = StackOrientation.Vertical,
                    VerticalOptions = LayoutOptions.Center,
                    Children =
                    {
                    new Label { Text = item.productname, Font=Font.SystemFontOfSize(NamedSize.Medium)
                        .WithAttributes (FontAttributes.Bold) },
                    l_priceOfProduct
                    }
                };
                row.Children.Add(nameLayout);
                row.Children.Add(new Button
                {
                    Text = item.pieces + "",
                    Font = Font.SystemFontOfSize(NamedSize.Small),
                    BorderColor = Color.Gray,
                    BorderWidth = 1,
                    WidthRequest = 60,
                    TextColor = Color.Gray,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    VerticalOptions = LayoutOptions.Center
                });
                mainProductLayout.Children.Add(row);
                //Border
                mainProductLayout.Children.Add(new BoxView { BackgroundColor = Color.Gray, HeightRequest = 1, VerticalOptions = LayoutOptions.Start, HorizontalOptions = LayoutOptions.FillAndExpand });
            }

            StackLayout ges = new StackLayout
            {
                Children =
                    {
                        mainScrollView,
                        btn_goToOrder,
                        new Grid{VerticalOptions=LayoutOptions.FillAndExpand,HorizontalOptions=LayoutOptions.FillAndExpand},
                        footerGrid
                    }
            };
            Content = ges;
        }
        public async System.Threading.Tasks.Task changeOrderFormularPageAsync()
        {
            App.Current.Properties["updateWindow"] = false;
            await Navigation.PushAsync(new OrderFormular());
        }
    }
}