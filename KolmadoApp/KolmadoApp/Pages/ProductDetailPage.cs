﻿using KolmadoApp.Controls;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace KolmadoApp
{
	public class ProductDetailPage : ContentPage
	{
		public ProductDetailPage (Image productImage, Product productToShow)
		{
            #region footer
            
            ImageButton homeIcon = new ImageButton { ShowedImage = "home.png", ShowedText = "Home" };
            ImageButton ordersIcon = new ImageButton { ShowedImage = "orders.png", ShowedText = "Orders", HeightRequest = 35 };
            ImageButton contactIcon = new ImageButton { ShowedImage = "contact.png", ShowedText = "Contact", HeightRequest = 35 };
            homeIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new MainPage());
                },
                NumberOfTapsRequired = 1
            });
            ordersIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    List<Product> orderProducts = HelpingFunctions.getProductsFromProperties("OrderProducts");
                    App.Current.Properties["OrderProducts"] = orderProducts;
                    if (orderProducts.Count > 0)
                    {
                        Navigation.PushAsync(new DeliveryPage());
                    }
                    else
                    {
                        DisplayAlert("Keine Bestellungen", "Leider haben Sie noch keine Bestellungen getätigt.", "OK");
                    }
                },
                NumberOfTapsRequired = 1
            });
            contactIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new ContactPage());
                },
                NumberOfTapsRequired = 1
            });

            var footerGrid = new Grid();
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.Children.Add(homeIcon, 0, 0);
            footerGrid.Children.Add(ordersIcon, 1, 0);
            footerGrid.Children.Add(contactIcon, 2, 0);
            #endregion
            ToolbarItem shoppingCartCounterTI = new ToolbarItem
            {
                Command = new Command(async () =>
await Navigation.PushAsync(new ShoppingCartPage())),
                Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + ""
            };
            var shoppingCartIconTI = new ToolbarItem
            {
                Command = new Command(async () =>
                await Navigation.PushAsync(new ShoppingCartPage())),
                Icon = "shoppingCart.png",
                Name = "Einkaufswagen"
            };
            ToolbarItems.Add(shoppingCartIconTI);
            ToolbarItems.Add(shoppingCartCounterTI);
            this.Padding = new Thickness(15, 30, 20, 0);
            var grid = new Grid { VerticalOptions = LayoutOptions.FillAndExpand };
            productImage.VerticalOptions = LayoutOptions.FillAndExpand;
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(3, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            grid.Children.Add(productImage,0,1);
            Label l_name = new Label {FontAttributes=FontAttributes.Bold, Text = productToShow.productname, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) };
            Label l_price = new Label {TextColor=Color.DarkGreen, Text=String.Format("{0:0.00}", Double.Parse(productToShow.price+"")) + " Euro", VerticalTextAlignment = TextAlignment.Center, HorizontalTextAlignment = TextAlignment.Center, FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) };
            Label l_description = new Label { Text = productToShow.description, HorizontalTextAlignment = TextAlignment.Center,VerticalTextAlignment = TextAlignment.Center, FontSize =Device.GetNamedSize(NamedSize.Large,typeof(Label))};
            grid.Children.Add(l_name, 0, 0);
            grid.Children.Add(l_price, 0,2);
            grid.Children.Add(l_description,0,3);
            Content = new StackLayout
            {
                Children =
                {
                    grid,
                    //new Grid{VerticalOptions=LayoutOptions.FillAndExpand},
                    footerGrid
                }
            };
        }
	}
}