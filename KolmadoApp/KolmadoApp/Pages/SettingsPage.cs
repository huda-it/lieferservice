﻿using KolmadoApp.Controls;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace KolmadoApp
{
	public class SettingsPage : ContentPage
	{
		public SettingsPage ()
        {
            this.Padding = new Thickness(15, 30, 20, 0);
            Title = "Einstellungen";
            #region footer
            
            ImageButton homeIcon = new ImageButton { ShowedImage = "home.png", ShowedText = "Home" };
            ImageButton ordersIcon = new ImageButton { ShowedImage = "orders.png", ShowedText = "Orders", HeightRequest = 35 };
            ImageButton contactIcon = new ImageButton { ShowedImage = "contact.png", ShowedText = "Contact", HeightRequest = 35 };
            homeIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new MainPage());
                },
                NumberOfTapsRequired = 1
            });
            ordersIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    List<Product> orderProducts = HelpingFunctions.getProductsFromProperties("OrderProducts");
                    App.Current.Properties["OrderProducts"] = orderProducts;
                    if (orderProducts.Count > 0)
                    {
                        Navigation.PushAsync(new DeliveryPage());
                    }
                    else
                    {
                        DisplayAlert("Keine Bestellungen", "Leider haben Sie noch keine Bestellungen getätigt.", "OK");
                    }
                },
                NumberOfTapsRequired = 1
            });
            contactIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new ContactPage());
                },
                NumberOfTapsRequired = 1
            });

            var footerGrid = new Grid();
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.Children.Add(homeIcon, 0, 0);
            footerGrid.Children.Add(ordersIcon, 1, 0);
            footerGrid.Children.Add(contactIcon, 2, 0);
            #endregion
            ToolbarItem shoppingCartCounterTI = new ToolbarItem
            {
                Command = new Command(async () =>
await Navigation.PushAsync(new ShoppingCartPage())),
                Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + ""
            };
            var shoppingCartIconTI = new ToolbarItem
            {
                Command = new Command(async () =>
                await Navigation.PushAsync(new ShoppingCartPage())),
                Icon = "shoppingCart.png",
                Name = "Einkaufswagen"
            };
            ToolbarItems.Add(shoppingCartIconTI);
            ToolbarItems.Add(shoppingCartCounterTI);
            var grid = new Grid { VerticalOptions=LayoutOptions.FillAndExpand,HorizontalOptions=LayoutOptions.FillAndExpand, BackgroundColor=Color.Black,
                RowSpacing=3
            };
            /* Label l_contact = new Label { Text = "Mein Account", HorizontalOptions = LayoutOptions.FillAndExpand, BackgroundColor = Color.AliceBlue, FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) };
             Label l_account = new Label { Text = "Kontakt", HorizontalOptions = LayoutOptions.FillAndExpand, BackgroundColor = Color.AliceBlue, FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) };
             grid.Children.Add(l_contact, 0, 1);
             grid.Children.Add(l_account, 0, 0);
             ScrollView mainLayoutScroll = new ScrollView { Content = grid };*/
            List<String> settingsRowsList = new List<string>() { "Mein Account", "Kontakt" };
            ListView categoryList = new ListView
            {
                ItemsSource = settingsRowsList,
                ItemTemplate = new DataTemplate(() =>
                {
                    Label nameLabel = new Label
                    {
                        TextColor = Color.Black
                    };
                    nameLabel.SetBinding(Label.TextProperty, ".");
                    // Source of data items.
                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            Padding = new Thickness(13, 5),
                            Orientation = StackOrientation.Horizontal,
                            Children =
                                    {
                                         new StackLayout
                                         {
                                             VerticalOptions = LayoutOptions.Center,
                                             Spacing = 0,
                                             Children =
                                             {
                                                 nameLabel
                                             }}
                                    }
                        }
                    };
                })
            };
            categoryList.ItemSelected += async (sender, e) => {
                ((ListView)sender).SelectedItem = null;
                string item = e.SelectedItem+"";
                if (item.Equals("Mein Account")) await Navigation.PushAsync(new AccountPage());
                if (item.Equals("Kontakt")) await Navigation.PushAsync(new ContactPage());
            };
            this.Content = new StackLayout
            {
                Children =
                     {
                   // headerLayout,
                         categoryList,
                         footerGrid
                     }
            };
        }
    }
}