﻿using KolmadoApp.Controls;
using Plugin.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace KolmadoApp
{
    public class DeliveryPage : ContentPage
    {
        public DeliveryPage()
        {
            #region footer
            
            ImageButton homeIcon = new ImageButton { ShowedImage = "home.png", ShowedText = "Home" };
            ImageButton ordersIcon = new ImageButton { ShowedImage = "orders.png", ShowedText = "Orders", HeightRequest = 35 };
            ImageButton contactIcon = new ImageButton { ShowedImage = "contact.png", ShowedText = "Contact", HeightRequest = 35 };
            homeIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new MainPage());
                },
                NumberOfTapsRequired = 1
            });
            ordersIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    List<Product> orderProducts = HelpingFunctions.getProductsFromProperties("OrderProducts");
                    App.Current.Properties["OrderProducts"] = orderProducts;
                    if (orderProducts.Count > 0)
                    {
                        Navigation.PushAsync(new DeliveryPage());
                    }
                    else
                    {
                        DisplayAlert("Keine Bestellungen", "Leider haben Sie noch keine Bestellungen getätigt.", "OK");
                    }
                },
                NumberOfTapsRequired = 1
            });
            contactIcon.GestureRecognizers.Add(new TapGestureRecognizer
            {
                TappedCallback = async (v, o) =>
                {
                    await Navigation.PushAsync(new ContactPage());
                },
                NumberOfTapsRequired = 1
            });

            var footerGrid = new Grid();
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            footerGrid.Children.Add(homeIcon, 0, 0);
            footerGrid.Children.Add(ordersIcon, 1, 0);
            footerGrid.Children.Add(contactIcon, 2, 0);
            #endregion
            ToolbarItem shoppingCartCounterTI = new ToolbarItem
            {
                Command = new Command(async () =>
await Navigation.PushAsync(new ShoppingCartPage())),
                Text = CrossSettings.Current.GetValueOrDefault("shoppingCartCounter", 0) + ""
            };
            var shoppingCartIconTI = new ToolbarItem
            {
                Command = new Command(async () =>
                await Navigation.PushAsync(new ShoppingCartPage())),
                Icon = "shoppingCart.png",
                Name = "Einkaufswagen"
            };
            ToolbarItems.Add(shoppingCartIconTI);
            ToolbarItems.Add(shoppingCartCounterTI);
            NavigationPage.SetBackButtonTitle(this, "Ihre Lieferungen");
            Title = "Ihre Lieferungen";
            int piecesGes = 0;
            double priceGes = 0;
            
            this.Padding = new Thickness(10, 10, 10, 0);
            var grid = new Grid()
            {
                BackgroundColor = Color.Black,
                Padding = new Thickness(3, 3, 3, 3),
                ColumnSpacing = 1
            };
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(4, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2, GridUnitType.Star) });

            StackLayout mainLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Padding = new Thickness(10, 10, 10, 10)
            };
            grid.Children.Add(new Label { BackgroundColor = Color.AliceBlue, Text = "  Stück", FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)), FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, VerticalTextAlignment = TextAlignment.Start }, 1, 0);
            grid.Children.Add(new Label { BackgroundColor = Color.AliceBlue, Text = "  Artikel", FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)), FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, VerticalTextAlignment = TextAlignment.Start }, 0, 0);
            grid.Children.Add(new Label { BackgroundColor = Color.AliceBlue, Text = "  Preis", FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)), FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, VerticalTextAlignment = TextAlignment.Start }, 2, 0);
            //grid.Children.Add(new Label { BackgroundColor = Color.White, Text = "  Kassieren", FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)), FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, VerticalTextAlignment = TextAlignment.Start }, 3, 0);
            //grid.Children.Add(new Label { BackgroundColor = Color.White, Text = "  Plus", FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)), FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, VerticalTextAlignment = TextAlignment.Start }, 4, 0);
            int rowCounter = 1;
            List<Product> orderProductList = (List<Product>)App.Current.Properties["OrderProducts"];
            foreach (Product item in orderProductList)
            {
                Label l_pieces = new Label { BackgroundColor = Color.AliceBlue, Text = "" + item.pieces, FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)), HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center };
                Label l_price = new Label { BackgroundColor = Color.AliceBlue, Text = item.price + " €", FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)), HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center };
                grid.Children.Add(l_pieces, 1, rowCounter);
                grid.Children.Add(new Label { BackgroundColor = Color.AliceBlue, Text = "  " + item.productname, FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)), HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, VerticalTextAlignment = TextAlignment.Center }, 0, rowCounter);
                Button btn_plus = new Button
                {
                    BackgroundColor = Color.White,
                    Text = "+",
                    FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Button)),
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    ClassId = rowCounter + ""
                };
                //grid.Children.Add(btn_minus, 2, rowCounter);
                grid.Children.Add(l_price, 2, rowCounter);
                //grid.Children.Add(btn_plus, 4, rowCounter);

                //End
                rowCounter++;
                priceGes += item.price * item.pieces;
                piecesGes += item.pieces;
            };
            //Column Clicked
             grid.Children.Add(new Label {BackgroundColor=Color.AliceBlue, Text ="Gesamt:", FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)), FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center }, 0, rowCounter);
            grid.Children.Add(new Label { BackgroundColor = Color.AliceBlue, Text = priceGes + " €", FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)), FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, VerticalTextAlignment = TextAlignment.Center, HorizontalTextAlignment = TextAlignment.Center }, 2, rowCounter);
            grid.Children.Add(new Label {BackgroundColor=Color.AliceBlue,  Text =  piecesGes + "", FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)), FontAttributes = FontAttributes.Bold, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center }, 1, rowCounter);
            var platzHalter = new Grid
            {
                VerticalOptions=LayoutOptions.FillAndExpand,
                BackgroundColor=Color.AliceBlue
            };
            ScrollView mainLayoutScroll = new ScrollView { Content = grid };
            Content = new StackLayout
            {
                Children = {
                    mainLayoutScroll,
                    platzHalter,
                    footerGrid
                }
            };

        }
    }
}

