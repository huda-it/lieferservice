﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KolmadoApp
{
    class GlobalVars
    {
        public static string host { get; set; } = "http://thagda.bplaced.net/Kolmado/";
        public static string personXml { get; set; } = "customer.xml";
        public static string productsXml { get; set; } = "categories.xml";
        public static List<Product> listOfAllProducts = new List<Product>();
        public static List<CategoriesAndProducts> allCategoriesGlobal = new List<CategoriesAndProducts>();
        
    }
}
