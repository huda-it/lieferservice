﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KolmadoApp
{
    public class Product
    {
        public String productname { get; set; }
        public double price { get; set; }
        public int piecesTotal { get; set; }
        public int pieces { get; set; } = 1;
        public int fsk { get; set; }
        public string description { get; set; }
        public double pfand
        {
            get;
            set;
        } = 0;
        public string pictureUrl { get; set; }

        public String ProductToXml()
        {
            String result = "<product>";
            result += "<name>" + this.productname + "</name>";
            result += "<pieces>" + this.pieces + "</pieces>";
            result += "<price>" + (this.price + this.pfand) + "</price>";
            result += "<fsk>" + this.fsk + "</fsk>";
            result += "</product>";
            return result;
        }
    }
}
