﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace KolmadoApp
{
    public class Person
    {
        public String id { get; set; }
        public String vorname { get; set; }
        public String nachname { get; set; }
        //eventuell public String password { get; set; } 
        public String ort { get; set; }
        public String straße { get; set; }
        public String hausnummer { get; set; }
        public String tel { get; set; }
        public int alter { get; set; }
        public String ip { get; set; }
        public String lieferzeit { get; set; }
        public PersonDeviceInfo deviceInfo { get; set; }
        public List<Product> productsOfPerson { get; set; }
        public Person()
        {
            deviceInfo = new PersonDeviceInfo();
        }
        public static XElement createXElementProductFromXmlString(string productsInXml)
        {
            XDocument doc = XDocument.Parse(productsInXml);
            XElement root = (XElement)doc.FirstNode;
            string a = root.Name + "";
            IEnumerable<XElement> list = root.XPathSelectElements("product");
            foreach (XElement element in list)
            {
                var name = element.XPathSelectElement("name");
                var pieces = element.XPathSelectElement("pieces");
                
            }
            return null;
        }

        XElement createXElementPersonProducts(String id, List<Product> products)
        {
            return null;
        }

        public bool addPersonToPersons(Person personToAdd)
        {
            
            XDocument doc = XDocument.Load(GlobalVars.host + GlobalVars.personXml);
            XElement root = (XElement)doc.FirstNode;
            //MessageBox.Show(root.Name+"");
            XElement person = createXElementPerson(personToAdd);
            person.Add(new XAttribute("id", personToAdd.id));
            root.Add(person);
            string xmlText = doc.ToString();
            string x = ProductListMethods.GetXmlFromProducts(this.productsOfPerson);
            xmlText += ProductListMethods.GetXmlFromProducts(this.productsOfPerson);
            return HttpRequests.changeXmlFilePhpRequest(GlobalVars.host, GlobalVars.personXml,xmlText);
        }
        public void changePersonFromPersons(String personIdToChange, String objectNameToChange, Object contentToInsert)
        {
            XDocument doc = XDocument.Load(@"C:\Users\daniel.hufnagl\Desktop\Neuer Ordner\xml1.xml");
            XElement root = (XElement)doc.FirstNode;
            IEnumerable<XElement> list = root.XPathSelectElements("/person[@id='Karl May'/ort");
            foreach (XElement el in list)
                Console.WriteLine(el);

            doc.Save(@"C:\Users\daniel.hufnagl\Desktop\Neuer Ordner\xml1.xml");
        }
        XElement createXElementPerson(Person person)
        {
            return new XElement("person",
                new XElement("vorname", person.vorname),
                new XElement("nachname", person.nachname),
                new XElement("alter",person.alter),
                new XElement("ort", person.ort),
                new XElement("strasse", person.straße),
                new XElement("hausnummer", person.hausnummer),
                new XElement("tel", person.tel),
                new XElement("zeit",person.lieferzeit),
                new XElement("deviceInfo",
                    new XElement("deviceId", person.deviceInfo.deviceId+""),
                    new XElement("deviceName", person.deviceInfo.deviceName)
                    )
            );
        }

    }
}
