﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace KolmadoApp
{
    public class CategoriesAndProducts
    {
        public string categoryName { get; set; }
        public Image categoryImage { get; set; }
        public List<Product> products { get; set; }
    }
}
