﻿using System;
using System.Collections.Generic;
using System.Text;
namespace KolmadoApp
{
    public class PersonDeviceInfo
    {
        public string deviceName { get; set; }
        public string deviceId { get; set; }
        public PersonDeviceInfo()
        {
        }
    }
}
