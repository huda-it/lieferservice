﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace KolmadoApp
{
    public class MainTabbedPageCS : TabbedPage
    {
        public MainTabbedPageCS(Page pageToShow)
        {
            var footer = pageToShow;

            Children.Add(footer);
            Children.Add(new ShoppingCartPage());
        }
    }
}
