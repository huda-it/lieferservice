﻿using PayPal.Forms.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace KolmadoApp
{
    public interface PaypalPreferences
    {
       int PaypalPage(PayPalItem[] paypalitems);
    }
}
